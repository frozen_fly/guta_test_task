module.exports = {
  publicPath: '',
  lintOnSave: true,
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/styles/global.scss";
          @import "@/styles/fonts.scss";
          @import "@/styles/variables.scss";
        `
      }
    }
  }
};